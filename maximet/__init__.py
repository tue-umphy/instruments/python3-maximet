# system modules
import logging
import time
import io
import threading
import csv
import operator
from functools import partial
import collections
import re
from contextlib import contextmanager

# internal modules
import maximet.version
from maximet.version import __version__

# external modules
import serial
import attr

logger = logging.getLogger(__name__)


class Error(Exception):
    """
    Base class for MaxiMet errors
    """

    pass


class Timeout(Error):
    pass


class CliError(Error):
    """
    Base class for MaxiMet command-line errors
    """

    @staticmethod
    def match(s):
        for cls in CliError.__subclasses__():
            if not hasattr(cls, "pattern"):
                continue
            if cls.pattern.search(s):
                yield cls


class IllegalCommandLine(CliError):
    pattern = re.compile(r"illegal\s*commandline", re.IGNORECASE)


class UnnecessaryCommand(CliError):
    pattern = re.compile(r"unnecessary\s*command", re.IGNORECASE)


class IncorrectParameters(CliError):
    pattern = re.compile(r"incorrect\s*paramters", re.IGNORECASE)


class OperatingMode:
    """
    Base class for MaxiMet operating modes
    """


class SetupMode(OperatingMode):
    pass


class MeasurementMode(OperatingMode):
    pass


class MessageMode:
    """
    Base class for MaxiMet message modes
    """

    pass


class PollMode(MessageMode):
    pass


class ContinuousMode(MessageMode):
    pass


def subclass_of(cls):
    def validator(param, attribute, value):
        return issubclass(value, cls)

    return validator


@attr.s
class MaxiMet:
    port = attr.ib(
        validator=attr.validators.instance_of(serial.Serial),
        default=attr.Factory(serial.Serial),
    )
    cmd_buffer = attr.ib(
        validator=attr.validators.instance_of(collections.deque),
        default=attr.Factory(lambda: collections.deque(maxlen=100)),
    )
    measurement_buffer = attr.ib(
        validator=attr.validators.instance_of(collections.deque),
        default=attr.Factory(lambda: collections.deque(maxlen=100)),
    )
    stopevent = attr.ib(
        validator=attr.validators.instance_of(threading.Event),
        default=attr.Factory(threading.Event),
    )
    operating_mode = attr.ib(
        validator=subclass_of(OperatingMode), default=OperatingMode
    )
    message_mode = attr.ib(validator=subclass_of(MessageMode), default=MessageMode)
    timeout = attr.ib(converter=float, validator=lambda p, a, v: v >= 0, default=2)
    columns = attr.ib(converter=tuple, default=tuple())

    thread = attr.ib(validator=attr.validators.instance_of(threading.Thread))

    @thread.default
    def create_thread(self):
        return threading.Thread(target=self.loop, daemon=True)

    skip_regexes = (re.compile(r">\s*.*"), re.compile(r"\s*"))

    def enter_setup_mode(self, force=False):
        """
        Enter setup mode on the device

        Args:
            force (bool, optional): whether to force changing mode

        Raises:
            CliError: if an error was responded
        """
        if self.operating_mode is SetupMode and not force:
            logger.debug(
                "As far as we know, "
                "we're already in setup mode, "
                "so no need to change to it"
            )
            return
        logger.debug("Entering SETUP mode")
        self.operating_mode = OperatingMode
        self.issue("*")
        try:
            self.wait_for_cmd_response(r"setup\s*mode")
        except UnnecessaryCommand:
            self.operating_mode = SetupMode
            raise
        self.operating_mode = SetupMode

    def exit_setup_mode(self, force=False):
        """
        Exit setup mode (thus enter measurement mode)

        Args:
            force (bool, optional):  whether to force
        """
        if self.operating_mode is not SetupMode and not force:
            logger.debug(
                "As far as we know, "
                "we're in {mode}, so no need to exit setup mode".format(
                    mode=self.operating_mode.__name__
                )
            )
            return
        logger.debug("Exiting SETUP mode")
        self.issue("EXIT", newline=True)
        self.operating_mode = MeasurementMode

    @property
    @contextmanager
    def setup_mode(self):
        """
        Context manager to enter and exit setup mode
        """
        previous_mode = self.operating_mode
        try:
            self.enter_setup_mode()
        except UnnecessaryCommand as e:
            previous_mode = SetupMode
            logger.debug(
                "Device responded {etype} which means "
                "we were already in setup mode.".format(etype=type(e).__name__)
            )
        try:
            yield
        finally:
            if previous_mode is SetupMode:
                logger.debug(
                    "We were already in setup mode before. " "No need to exit it now."
                )
            else:
                self.exit_setup_mode()

    def wait_for_cmd_response(self, pattern, timeout=None):
        """
        :any:`deque.popleft` from the :any:`cmd_buffer` until a given pattern
        matches or a timeout is reached

        Raises:
            Timeout : on timeout
            CliError : if the device responded with an error
        """
        if not timeout:
            timeout = self.timeout
        time_before = time.time()
        regex = (
            pattern
            if hasattr(pattern, "search")
            else re.compile(pattern, re.IGNORECASE)
        )
        while True:
            if self.stopevent.is_set():
                logger.debug("stop waiting for data")
                break
            if timeout:
                if time.time() - time_before > timeout:
                    raise Timeout(
                        "Device didn't respond any command answer "
                        "looking like {pattern} "
                        "within {timeout:.2f} seconds".format(
                            pattern=regex.pattern, timeout=timeout
                        )
                    )
            if not self.cmd_buffer:
                continue
            line = self.cmd_buffer.popleft()
            error = next(CliError.match(line), None)
            if error:
                raise error(line)
            m = regex.search(line)
            if m:
                return m
            logger.info(
                "Dropping input {line} while "
                "waiting for an answer looking like {pattern}".format(
                    line=repr(line), pattern=regex.pattern
                )
            )

    def issue(self, s, newline=True):
        """
        Send a command to the device (send the string + ENTER)
        """
        self.port.write("\r\n".encode(errors="ignore"))
        self.port.write(s.encode(errors="ignore"))
        if newline:
            self.port.write("\r\n".encode(errors="ignore"))
        self.port.flush()

    def get_available_columns(self):
        old_columns = self.get_columns()
        with self.setup_mode:
            self.issue("report full")
            self.wait_for_cmd_response(r"report\s*full")
            self.issue("report")
            match = self.wait_for_cmd_response(
                r"report\s*=\s*(?P<columns>(?:\w+)(?:\s*,\s*\w+)*)"
            )
            available_columns = re.split(
                r"\s*,\s*", match.groupdict().get("columns", "")
            )
        self.set_columns(old_columns)
        return available_columns

    def get_columns(self):
        with self.setup_mode:
            self.issue("report")
            match = self.wait_for_cmd_response(
                r"report\s*=\s*(?P<columns>(?:\w+)(?:\s*,\s*\w+)*)"
            )
            self.columns = re.split(r"\s*,\s*", match.groupdict().get("columns", ""))
            return self.columns

    def set_columns(self, columns):
        with self.setup_mode:
            self.issue("report {columns}".format(columns=",".join(columns)))
            match = self.wait_for_cmd_response(
                r"report\s*=\s*(?P<columns>(?:\w+)(?:\s*,\s*\w+)*)"
            )
            new_columns = re.split(r"\s*,\s*", match.groupdict().get("columns", ""))
            if set(new_columns) < set(columns):
                logger.warning(
                    "Device didn't include columns {excluded}".format(
                        excluded=", ".join(set(columns) - set(new_columns))
                    )
                )
            self.columns = new_columns

    @property
    def data(self):
        """
        Generator yielding the next queued dataset and removing it from the
        :any:`measurement_buffer`
        """
        self.exit_setup_mode()

        def to_float(x):
            try:
                return float(x)
            except BaseException:
                return x if x else None

        while True:
            if self.stopevent.is_set():
                logger.debug("stop waiting for data")
                break
            if self.columns:
                if self.measurement_buffer:
                    line = self.measurement_buffer.popleft()
                    reader = csv.reader((line,))
                    row = next(reader)
                    yield dict(
                        filter(
                            lambda x: x[1] is not None,
                            zip(self.columns, map(to_float, row)),
                        )
                    )
                else:
                    time.sleep(0.1)
            else:
                self.get_columns()

    def start(self):
        """
        Start running and init :any:`csvreader`
        """
        self.csvreader = csv.DictReader(
            self.measurement_buffer, fieldnames=self.columns
        )
        self.thread.start()

    def stop(self):
        """
        Stop running
        """
        self.stopevent.set()
        logger.debug("Waiting for input thread to finish...")
        self.thread.join()
        logger.debug("Input thread finished")

    def loop(self):
        """
        Start background activity: parse from the :any:`port` and
        fill :any:`cmd_buffer` and :any:`measurement_buffer` accordingly
        """
        logger.debug("Start reading from device")
        self.stopevent.clear()
        while True:
            if self.stopevent.is_set():
                logger.debug("Stop reading from device")
                break
            try:
                in_waiting = self.port.in_waiting
            except (OSError, serial.SerialException) as e:
                logger.error(
                    "{etype.__name__} while checking queued bytes from device "
                    "'{port.name}': {error}".format(
                        etype=type(e), error=e, port=self.port
                    )
                )
                self.stopevent.set()
                break
            if not in_waiting:
                time.sleep(0.1)
                continue
            try:
                line = next(self.port)
            except (OSError, serial.SerialException, StopIteration) as e:
                logger.error(
                    "{etype.__name__} while reading from device "
                    "'{port.name}': {error}".format(
                        etype=type(e), error=e, port=self.port
                    )
                )
                self.stopevent.set()
                break
            line = line.decode(errors="ignore")
            line = re.sub(r"^\s*(.*?)\s*$", r"\1", line)
            if any(map(operator.methodcaller("fullmatch", line), self.skip_regexes)):
                continue
            logger.debug("Recieved {line} from device".format(line=repr(line)))
            if self.operating_mode in (SetupMode, OperatingMode):
                self.cmd_buffer.append(line)
            elif self.operating_mode is MeasurementMode:
                self.measurement_buffer.append(line)
            else:
                logger.warning(
                    "Skipping incoming line {line} in "
                    "operating mode {mode.__name__}: "
                    "Don't know what do do with it!".format(
                        line=line, mode=self.operating_mode
                    )
                )
        logger.debug("loop exited")

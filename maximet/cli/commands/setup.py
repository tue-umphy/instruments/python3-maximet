# system modules
import logging

# internal modules
from maximet.cli.commands.main import cli

# external modules
import click

logger = logging.getLogger(__name__)


@cli.command(
    help="Configure parameters of a MaxiMet device",
    context_settings={"help_option_names": ["-h", "--help"]},
)
@click.pass_context
def setup(ctx):
    ctx.fail("Configuring is not yet implemented!")

# system modules
import logging

# internal modules
from maximet import MaxiMet

# external modules
import click
import serial
from serial.tools import list_ports

logger = logging.getLogger(__name__)


def find_maximet_port():
    """
    lists ``ttyACM*`` and ``ttyUSB*`` ports and puts those with vendor id
    ``0x0403`` and product id ``0x6001`` first.
    """
    port = next(
        iter(
            sorted(
                list_ports.grep(r"tty(usb|acm)"),
                key=lambda x: ((x.vid if x.vid else 0) == 0x0403)
                + ((x.pid if x.pid else 0) == 0x6001),
                reverse=True,
            )
        ),
        None,
    )
    if port:
        return port.device
    else:
        raise click.UsageError(
            "Cannot determine MaxiMet port automatically. "
            "Please specify a port via -p/--port."
        )


@click.group(
    help="Interfacing GILL MaxiMet compact weather stations via USB",
    context_settings={
        "help_option_names": ["-h", "--help"],
        "auto_envvar_prefix": "MAXIMET",
    },
)
@click.option(
    "-p",
    "--port",
    help="device to connect to",
    default=find_maximet_port,
    show_default="loosely matching serial port",
)
@click.option(
    "-b",
    "--baudrate",
    help="baudrate to connect with",
    type=click.IntRange(min=1),
    default=19200,
    show_default=True,
)
@click.option(
    "--timeout",
    help="timeout for communication in seconds. 0 means infinite.",
    type=click.FloatRange(min=0),
    default=5,
    show_default=True,
)
@click.option("-q", "--quiet", help="only show warnings", is_flag=True)
@click.option("-v", "--verbose", help="verbose output", is_flag=True)
@click.version_option(help="show version and exit")
@click.pass_context
def cli(ctx, quiet, verbose, port, baudrate, timeout):
    # set up logging
    loglevel = logging.DEBUG if verbose else logging.INFO
    loglevel = logging.WARNING if quiet else loglevel
    logging.basicConfig(
        level=loglevel, format="[%(asctime)s] - %(levelname)-8s - %(message)s"
    )
    for n, l in logger.manager.loggerDict.items():
        if not n.startswith("maximet"):
            l.propagate = False
    ctx.ensure_object(dict)
    device = serial.Serial()
    device.port = port
    device.baudrate = baudrate
    logger.debug(
        "Opening device {device.port}@{device.baudrate}baud".format(device=device)
    )
    try:
        device.open()
    except BaseException as e:
        ctx.fail(
            "Couldn't open device {device.port}@{device.baudrate}baud: "
            "{etype}: {error}".format(device=device, etype=type(e).__name__, error=e)
        )
    ctx.call_on_close(device.close)
    maximet_device = MaxiMet(port=device, timeout=timeout)
    ctx.obj["device"] = maximet_device

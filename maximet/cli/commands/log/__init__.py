# system modules
import logging
import re
import csv
import io
from urllib.parse import urlparse, parse_qsl
import time
import datetime

# internal modules
from maximet.cli.commands.main import cli
from maximet.cli.commands.log.config import Configuration, BrokerSectionError

# external modules
import serial
import click

logger = logging.getLogger(__name__)


def scheme_url(scheme):
    def url(arg):
        return urlparse(
            arg
            if arg.startswith("{}://".format(scheme))
            else "{}://{}".format(scheme, arg),
            scheme=scheme,
        )

    return url


@cli.command(
    help="Log data from a MaxiMet device",
    context_settings={"help_option_names": ["-h", "--help"]},
)
@click.option(
    "-c",
    "--config",
    "configfiles",
    help="Add a configuration file to read. Can be specified multiple times.",
    type=click.Path(readable=True, exists=True, dir_okay=False),
    multiple=True,
    show_envvar=True,
    default=tuple(),
)
@click.option(
    "-o",
    "--output",
    "logfile",
    help="File to log CSV data into instead of stdout",
    type=click.File(mode="w", lazy=True),
    default="-",
)
@click.option(
    "-b",
    "--broker",
    "mqtt_brokers",
    help="add another MQTT broker to publish the data to. "
    "Supports user:pass@host:port/TOPIC?OPTIONS format, "
    "where TOPIC is a topic pattern like /topic/subtopic/{quantity} where "
    "{quantity} will be substituted with the field name "
    "(e.g. TEMP,SPEED, etc.). "
    "OPTIONS can be used to rename quantities to custom "
    "names, e.g. TEMP=temperature&AVGSPEED=wind would "
    "rename TEMP and AVGSPEED accordingly and also publish only those "
    "quantities to the broker. With _interval=SECONDS, the minimum "
    "interval between publishments to the broker can be specified. "
    "This option can be specified multiple times.",
    type=scheme_url("mqtt"),
    show_envvar=True,
    default=tuple(),
    multiple=True,
)
@click.pass_context
def log(ctx, mqtt_brokers, configfiles, logfile):
    ctx.ensure_object(dict)

    config = Configuration()
    logger.debug("Reading configuration files {}".format(configfiles))
    config.read(configfiles)

    maximet_device = ctx.obj["device"]

    for broker in mqtt_brokers:
        logger.debug("Adding {} to configuration".format(broker))
        section = config.add_section(
            "{prefix}:{hostname}".format(
                prefix=config.BROKER_SECTION_PREFIX, hostname=broker.hostname
            )
        )
        section["hostname"] = broker.hostname if broker.hostname else "localhost"
        section["topic"] = (
            re.sub(r"^/", r"", broker.path) if broker.path else "maximet/{quantity}"
        )
        if broker.query:
            mapping = dict(parse_qsl(broker.query, keep_blank_values=True))
            interval = mapping.pop("_interval", None)
            if interval is not None:
                try:
                    section["interval"] = str(float(interval))
                except (ValueError, TypeError) as e:
                    logger.error(
                        "Invalid interval {interval} "
                        "for broker {hostname}: {e}.".format(
                            interval=repr(interval),
                            hostname=section["hostname"],
                            e=e,
                        )
                    )
            section["rename"] = "\n".join(map("=".join, mapping.items()))
        for attrib in ("port", "username", "password"):
            value = getattr(broker, attrib, None)
            if value is not None:
                section[attrib] = str(value)

    buf = io.StringIO()
    config.write(buf)
    logger.debug("Configuration:\n{}".format(buf.getvalue().strip()))

    def start_client(client):
        logger.info(
            "Starting MQTT client {client._client_id} connecting to "
            "broker {client._host}:{client._port}...".format(client=client)
        )

        def on_connect(client, userdata, flags, rc):
            if rc:
                logger.error(
                    "Client {client._client_id} couldn't connect to "
                    "{client._host}:{client._port}: result code {rc}".format(
                        rc=rc, client=client
                    )
                )
            else:
                logger.info(
                    "Client {client._client_id} now connected to "
                    "{client._host}:{client._port}".format(client=client)
                )

        client.on_connect = on_connect
        client._last_publishment = 0
        client.loop_start()
        return client

    try:
        clients = tuple(map(start_client, config.clients))
    except BrokerSectionError as e:
        raise click.ClickException(
            "Error setting up MQTT clients from "
            "configuration: {error}".format(error=e)
        )

    def close_clients():
        for client in clients:
            logger.debug(
                "disconnecting MQTT client {client._client_id}".format(client=client)
            )
            client.disconnect()

    ctx.call_on_close(close_clients)
    ctx.call_on_close(maximet_device.stop)

    maximet_device.start()

    columns = maximet_device.get_columns()

    writer = csv.DictWriter(logfile, fieldnames=columns + ["SYSTEMTIME"])
    writer.writeheader()

    while True:
        maximet_device.thread.join(0.1)
        if not maximet_device.thread.is_alive():
            logger.error("MaxiMet device thread exited")
            break
        try:
            data = next(maximet_device.data)
        except StopIteration:
            logger.error("No more data from the device")
            break
        except (OSError, serial.SerialException):
            logger.error(
                "{etype.__name__} while retrieving data from device "
                "'{port.name}': {error}".format(
                    etype=type(e), error=e, port=maximet_device.port
                )
            )
            break
        data["SYSTEMTIME"] = (
            datetime.datetime.utcnow()
            .replace(tzinfo=datetime.timezone.utc)
            .strftime("%Y-%m-%dT%H:%M:%S%z")
        )
        logger.info("Saving dataset {}".format(data))
        writer.writerow(data)
        logfile.flush()
        for client in clients:
            if time.time() - client._last_publishment < client._publish_interval:
                logger.debug(
                    "Client {client._client_id} "
                    "on {client._host}:{client._port} still has to wait "
                    "{remaining:.2f}/{total:.2f} seconds "
                    "for next publishment".format(
                        client=client,
                        remaining=client._publish_interval
                        - (time.time() - client._last_publishment),
                        total=client._publish_interval,
                    )
                )
                continue
            for quantity, value in data.items():
                topic_template = getattr(
                    client, "_topic_template", "maximet/{quantity}"
                )
                if client._rename_mapping:
                    if quantity not in client._rename_mapping:
                        continue
                    quantity = client._rename_mapping[quantity]
                try:
                    topic = topic_template.format(quantity=quantity)
                except KeyError as e:
                    logger.error(
                        "Topic template {template} "
                        "contains unknown field {f}. "
                        "Using default template {default}".format(
                            template=repr(topic_template),
                            default=repr("maximet/{quantity}"),
                            f=repr(next(iter(e.args))),
                        )
                    )
                    topic = "maximet/{quantity}".format(quantity=quantity)
                msginfo = client.publish(topic, value)
                client._last_publishment = time.time()
                try:
                    if msginfo.is_published:
                        logger.info(
                            "Client {client._client_id} on "
                            "{client._host}:{client._port} "
                            "successfully published {msg} "
                            "to {topic}".format(msg=value, topic=topic, client=client)
                        )
                    else:
                        logger.debug(
                            "Client {client._client_id} on "
                            "{client._host}:{client._port} "
                            "queued {msg} for publishment to {topic}".format(
                                msg=value, topic=topic, client=client
                            )
                        )
                except ValueError as e:
                    logger.debug(
                        "Client {client._client_id} on "
                        "{client._host}:{client._port} "
                        "could not queue {msg} for "
                        "publishment to {topic}: {error}".format(
                            msg=value, topic=topic, error=e, client=client
                        )
                    )
    logger.warning("MaxiMet Thread exited")

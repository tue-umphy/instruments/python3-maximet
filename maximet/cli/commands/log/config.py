# system modules
import logging
import configparser
import random
import itertools
from functools import partial
import re
from urllib.parse import urlparse

# internal modules

# external modules
import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)


class ConfigurationError(ValueError):
    pass


class BrokerSectionError(ConfigurationError):
    pass


class Configuration(configparser.ConfigParser):
    """
    Class for configuration
    """

    BROKER_SECTION_PREFIX = "broker"
    """
    Prefix for sections specifying an MQTT broker
    """

    @property
    def broker_sections(self):
        """
        Generator yielding source sections

        Yields:
            configparser.SectionProxy: the next source section
        """
        for name, section in self.items():
            if name.startswith(self.BROKER_SECTION_PREFIX):
                yield section

    @property
    def clients(self):
        """
        Generator yielding set-up clients from the sections

        Yields:
            paho.mqtt.client.Client: the set-up but unstarted clients
        """
        for section in self.broker_sections:
            client_name = section.get(
                "client-name",
                "maximet-{}".format(random.randint(1, 2**16 - 1)),
            )
            client = mqtt.Client(client_name)
            client.username_pw_set(section.get("username"), section.get("password"))
            client._topic_template = section.get("topic", "maximet/{quantity}")
            client._publish_interval = section.getfloat("interval", 0)
            client._rename_mapping = (
                dict(
                    map(
                        partial(re.split, r"\s*=\s*", maxsplit=1),
                        re.split(r"\s*[\r\n]+\s*", section.get("rename", "")),
                    )
                )
                if "rename" in section
                else dict()
            )
            client.connect_async(
                section.get("hostname", "localhost"),
                section.getint("port", 1883),
            )
            yield client

    def add_section(self, name):
        """
        Add a new section with :any:`configparser.ConfigParser.add_section` but
        but if the name already exists, append something so that it doesn't
        exist.

        Args:
            name (str): the name of the new section

        Returns:
            Section : the newly created section
        """
        nonexistant_name = next(
            filter(
                lambda x: x not in self,
                itertools.chain(
                    (name,),
                    (
                        " ".join(map(str, (name, "nr.", nr)))
                        for nr in itertools.count(2)
                    ),
                ),
            )
        )
        configparser.ConfigParser.add_section(self, nonexistant_name)
        return self[nonexistant_name]

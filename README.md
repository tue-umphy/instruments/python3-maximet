# MaxiMet via Python

[![pipeline status](https://gitlab.com/tue-umphy/instruments/python3-maximet/badges/master/pipeline.svg)](https://gitlab.com/tue-umphy/instruments/python3-maximet/commits/master)
[![coverage report](https://gitlab.com/tue-umphy/instruments/python3-maximet/badges/master/coverage.svg)](https://tue-umphy/instruments.gitlab.io/python3-maximet/coverage-report/)
[![documentation](https://img.shields.io/badge/docs-sphinx-brightgreen.svg)](https://tue-umphy.gitlab.io/instruments/python3-maximet)
[![PyPI](https://badge.fury.io/py/maximet.svg)](https://badge.fury.io/py/maximet)

`maximet` is a Python package to interface GILL MaxiMet meteorological compact
weather stations.

## Installation

To install, run from the repository root:

```bash
python3 -m pip install --user .
```

(Run `sudo apt-get update && sudo apt-get -y install python3-pip` if it
complains about `pip` not being found)

## What can `maximet` do?

Nothing yet...

## Documentation

Documentation of the `maximet` package can be found [here on
GitLab](https://tue-umphy.gitlab.io/instruments/python3-maximet/).

Also, the command-line help page `maximet -h` is your friend.

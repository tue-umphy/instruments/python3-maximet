# system modules
import unittest

# internal modules
import maximet.cli.commands.main as main

# external modules
from click.testing import CliRunner


class CliTest(unittest.TestCase):
    def setUp(self):
        self.runner = CliRunner()


class GenerateTest(CliTest):
    def test_help(self):
        self.runner.invoke(main.cli)

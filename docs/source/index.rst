Welcome to maximet's documentation!
===================================

:mod:`maximet` is a Python package to interface `MaxiMet`_ compact weather
stations.

.. _MaxiMet: http://gillinstruments.com/products/anemometer/maximet-compact-weather-stations.html


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
